"""Tests for `url` module"""
# pylint: disable=redefined-builtin,pointless-statement
import pytest

import awesomeurl
from awesomeurl.exceptions import NotRelativeURL


@pytest.mark.parametrize(
    'input, scheme, user, password, host, port, path, query',
    (
        (None, None, None, None, None, None, None, None),
        ('', None, None, None, None, None, None, None),
        ('example.com', None, None, None, 'example.com', None, None, None),
        ('user@example', None, 'user', None, 'example', None, None, None),
        ('example:80', None, None, None, 'example', 80, None, None),
        ('path', None, None, None, None, None, 'path', None),
        ('/path', None, None, None, None, None, '/path', None),
        ('path/subpath', None, None, None, None, None, 'path/subpath', None),
        ('http://example.com', 'http', None, None, 'example.com', None, None, None),
        ('https://example.com', 'https', None, None, 'example.com', None, None, None),
        ('http://user@example.com', 'http', 'user', None, 'example.com', None, None, None),
        ('http://user:password@example.com', 'http', 'user', 'password', 'example.com', None, None, None),
        ('http://user:password@example.com:80', 'http', 'user', 'password', 'example.com', 80, None, None),
        ('http://user:password@example.com:80/path', 'http', 'user', 'password', 'example.com', 80, 'path', None),
        ('http://user:password@example.com:80//path', 'http', 'user', 'password', 'example.com', 80, 'path', None),
        ('http://user:password@example.com:80/path/subpath', 'http', 'user', 'password', 'example.com', 80,
         'path/subpath', None),
        ('http://user:password@example.com:80/path/subpath?key=value', 'http', 'user', 'password', 'example.com',
         80, 'path/subpath', 'key=value'),
        ('http://user:password@example.com:80/path/subpath?key=value&key1=value1', 'http', 'user', 'password',
         'example.com', 80, 'path/subpath', 'key=value&key1=value1'),
    ),
)
def test_parse_url(input: str, scheme: str | None, user: str | None, password: str | None, host: str | None,
                   port: int, path: str, query: str):
    url = awesomeurl.URL(input)
    assert url.scheme == scheme
    assert url.user == user
    assert url.password == password
    assert url.host == host
    assert url.port == port
    assert url.path == path
    assert url.query == query


@pytest.mark.parametrize(
    'input, output',
    (
        ('example.com', 'example.com'),
        ('http://example.com', 'http://example.com'),
        ('http://example.com:80', 'http://example.com:80'),
        ('https://example.com', 'https://example.com'),
        ('https://user@example.com', 'https://example.com'),
        ('https://user:password@example.com', 'https://example.com'),
        ('https://example.com/path', 'https://example.com'),
        ('https://example.com/path/subpath', 'https://example.com'),
        ('https://example.com/path/subpath?key=value', 'https://example.com'),
        ('https://example.com/path/subpath?key=value&key1=value1', 'https://example.com'),
    )
)
def test_parsed_base_url(input: str, output: str):
    url = awesomeurl.URL(input)
    assert url.base_url == output


@pytest.mark.parametrize(
    'input, output',
    (
        ({'host': 'example.com'}, 'example.com'),
        ({'host': 'example.com', 'port': 80}, 'example.com:80'),
        ({'scheme': 'http', 'host': 'example.com'}, 'http://example.com'),
        ({'scheme': 'https', 'host': 'example.com'}, 'https://example.com'),
        ({'user': 'user', 'host': 'example.com'}, 'example.com'),
        ({'host': 'example.com', 'path': 'path'}, 'example.com'),
        ({'host': 'example.com', 'path': 'path', 'query': 'key=value'}, 'example.com'),
    )
)
def test_generated_base_url(input: dict[str: str | int], output: str | None):
    url = awesomeurl.URL(**input)
    assert url.base_url == output


@pytest.mark.parametrize(
    'input, output',
    (
        ({'host': 'example.com'}, 'example.com'),
        ({'host': 'example.com', 'port': 80}, 'example.com:80'),
        ({'scheme': 'http', 'host': 'example.com'}, 'http://example.com'),
        ({'scheme': 'https', 'host': 'example.com'}, 'https://example.com'),
        ({'user': 'user', 'host': 'example.com'}, 'user@example.com'),
        ({'user': 'user', 'password': 'password', 'host': 'example.com'}, 'user:password@example.com'),
        ({'password': 'password', 'host': 'example.com'}, 'example.com'),
        ({'host': 'example.com', 'path': 'path'}, 'example.com/path'),
        ({'host': 'example.com/', 'path': 'path'}, 'example.com/path'),
        ({'host': 'example.com', 'path': 'path/subpath'}, 'example.com/path/subpath'),
        ({'host': 'example.com', 'path': 'path', 'query': 'key=value'}, 'example.com/path?key=value'),
        ({'query': 'key=value'}, ''),
        ({'path': 'path', 'query': 'key=value'}, 'path?key=value'),
        ({'host': 'example.com', 'query': 'key=value'}, 'example.com/?key=value'),
    )
)
def test_str(input, output):
    url = awesomeurl.URL(**input)
    assert str(url) == output


@pytest.mark.parametrize(
    'input, other, output',
    (
        ('example.com', '/path', 'example.com/path'),
        ('example.com', 'path', 'example.com/path'),
        ('http://example.com', '/path', 'http://example.com/path'),
        ('/', '/path', '/path'),
        ('root', '/path', 'root/path'),
        ('/root', '/path', '/root/path'),
        ('/?key=value', '/path?key=value', '/path?key=value&key=value'),
        ('/', '/path?key=value', '/path?key=value'),
    )
)
def test_add(input, other, output):
    url = awesomeurl.URL(input)
    url = url + other
    assert str(url) == output


def test_add_exception():
    url = awesomeurl.URL('/path')
    with pytest.raises(NotRelativeURL):
        url + 'example.com/path'


@pytest.mark.parametrize(
    'input, other, output',
    (
        ('example.com', '/path', 'example.com/path'),
        ('example.com', 'path', 'example.com/path'),
        ('http://example.com', '/path', 'http://example.com/path'),
        ('/', '/path', '/path'),
        ('root', '/path', 'root/path'),
        ('/root', '/path', '/root/path'),
        ('/?key=value', '/path?key=value', '/path?key=value&key=value'),
    )
)
def test_join(input, other, output):
    url = awesomeurl.join(input, other)
    assert str(url) == output


def test_join_exception():
    with pytest.raises(NotRelativeURL):
        awesomeurl.join('/path', 'example.com/path')


@pytest.mark.parametrize(
    'input, other, output',
    (
        ('example.com', '/path', 'example.com/path'),
        ('example.com', 'path', 'example.com/path'),
        ('http://example.com', '/path', 'http://example.com/path'),
        ('/', 'path', '/path'),
        ('root', 'path', 'root/path'),
        ('/root', 'path', '/root/path'),
        ('/?key=value', '/path?key=value', '/path?key=value&key=value'),
        ('/', '/path?key=value', '/path?key=value'),
    )
)
def test_truediv(input, other, output):
    url = awesomeurl.URL(input)
    url = url / other
    assert str(url) == output


def test_truediv_exception():
    url = awesomeurl.URL('/path')
    with pytest.raises(NotRelativeURL):
        url / 'example.com/path'


def test_multi_truediv():
    url = awesomeurl.URL('path')
    url = url / 'subpath' / 'subsubpath'
    assert str(url) == 'path/subpath/subsubpath'
