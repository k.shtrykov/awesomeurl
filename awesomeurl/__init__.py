""" Awesome URL

Because it's awesome!
Author: Kirill Shtrykov
Date: 2022-11-29
"""
__version__ = '0.1.0'

from typing import Tuple

from .url import URL, join

__all__: Tuple[str, ...] = ('URL', 'join',)
