""" Awesome URL

Because it's awesome!
Author: Kirill Shtrykov
Date: 2022-11-29
"""
from typing import Union

from .exceptions import NotRelativeURL

TABS_AND_NEWLINE = ['\t', '\n', '\r']
SCHEME_SEPARATOR = '://'


class URL:
    """ This is main library class that represent URL and operations with it """
    def __init__(self, url: str = None, *, scheme: str = None, user: str = None, password: str = None, host: str = None,
                 port: int = None, path: str = None, query: str = None, default_scheme: str = None):
        """
        URL constructor can receive as input both string and keywords arguments
        :param url: an url string, can be full or relative URL
        :param scheme: URL scheme
        :param user: URL user
        :param password: URL password
        :param host: URL host will be recognized if contains dot or URL contains user/password or port
        :param port: URL port
        :param path: URL relative path
        :param query: URL query
        :param default_scheme: can be set as default value if no schema is specified
        """
        if url:
            for c in TABS_AND_NEWLINE:
                url.replace(c, '')
            scheme_length = url.find(SCHEME_SEPARATOR)
            if scheme_length > 0:
                scheme = scheme or url[:scheme_length]
                url = url[scheme_length + len(SCHEME_SEPARATOR):]
            url = url.replace('//', '/')
            parts = url.split('/')
            is_host = False
            if '@' in parts[0]:
                is_host = True
                creds, netloc = parts[0].split('@')
                if ':' in creds:
                    password = password or creds.split(':')[1]
                    creds = creds.split(':')[0]
                user = user or creds
                parts[0] = netloc
            if ':' in parts[0]:
                is_host = True
                port = int(port) if port else int(parts[0].split(':')[1])
                parts[0] = parts[0].split(':')[0]
            host = host or parts.pop(0) if '.' in parts[0] or is_host else None
            if len(parts) > 0:
                if '?' in parts[-1]:
                    query = query or parts[-1].split('?')[1]
                    parts[-1] = parts[-1].split('?')[0]
                path = path or '/'.join(parts)
        self.scheme = scheme or default_scheme
        self.user = user
        self.password = password
        self.host = host
        self.port = port
        self.path = path
        self.query = query

    def __str__(self) -> str:
        """
        String representation of URL
        User, password and port will be skipped if not host defined
        """
        url = ''
        if self.host:
            if self.scheme:
                url += f'{self.scheme}://'
            url += self.host
            if self.port:
                url += f':{self.port}'
            if self.user:
                url = f'@{url}'
                if self.password:
                    url = f':{self.password}{url}'
                url = self.user + url
        if self.path:
            if len(url) > 0 and not self.path.startswith('/'):
                url = url.rstrip('/') + '/'
            url += self.path
        if self.query and len(url) > 0:
            if not url.endswith('/') and not self.path:
                url += '/'
            url += f'?{self.query}'
        return url

    def __add__(self, other: Union[str, 'URL']) -> 'URL':
        if isinstance(other, str):
            other = URL(other)
        if not isinstance(other, URL):
            return NotImplemented
        if other.host:
            raise NotRelativeURL('Path-only instance must be right side')
        self.path = self.path.rstrip('/') if self.path else ''
        self.path = self.path + '/' + other.path.lstrip('/')
        if self.path == '/':
            self.path = None
            return self
        if other.query:
            self.query = self.query + '&' if self.query else ''
            self.query += other.query
        return self

    def __truediv__(self, other: Union[str, 'URL']) -> 'URL':
        if isinstance(other, str):
            other = URL(other)
        if not isinstance(other, URL):
            return NotImplemented
        if other.host:
            raise NotRelativeURL('Path-only instance must be right side')
        self.path = self.path.rstrip('/') if self.path else ''
        self.path = self.path + '/' + other.path.lstrip('/')
        if self.path == '/':
            self.path = None
        if other.query:
            self.query = self.query + '&' if self.query else ''
            self.query += other.query
        return self

    @property
    def base_url(self) -> str:
        """ Returns scheme://host:port """
        url = f'{self.scheme}://{self.host}' if self.scheme else self.host
        if url and self.port:
            url += f':{self.port}'
        return url


def join(one: Union[str, 'URL'], other: Union[str, 'URL']) -> 'URL':
    if isinstance(one, str):
        one = URL(one)
    return one + other
